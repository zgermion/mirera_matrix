//
//  main.c
//  Mirera
//
//  Created by German Zelenin on 12.03.2020.
//  Copyright © 2020 German Zelenin. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int setMatrix(FILE *filein, int count, double *A){
    int j;
    for (j = 0; j<count; j = j + 1) {
        double number;
        int h = fscanf(filein, "%lf", &number);
        if(h!=EOF){
            if(h==1){
                A[j] = number;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
    
    return 0;
}

int findLine(double *A, int count, int stroka, int column){
    int a = 0;
    int MaxDiag = A[0];
    int i;
    int c = 0;
    for (i = 0; i<stroka && c<column; i = i + 1) {
        if(A[i*column+c]>MaxDiag){
            MaxDiag = A[i*column+c];
            a = i;
        }
        c = c + 1;
    }
    
    return a;
}

void MatrixTrans(double *A, int line, int column){
    int i1,k;
    for (i1 = line*column; i1>0; i1 = i1 - column) {
        for (k = 0; k<column; k = k+1) {
            A[i1+k] = A[i1+k-column];
        }
    }
    int y;
    for (y = 0; y<column; y = y + 1) {
        A[y] = 0;
    }
}

void MatrixEnter(FILE *fileout, int stroka, int column, double *A){
    int i, j;
    for (i = 0; i<stroka; i = i+1) {
        for (j = 0; j<column; j = j+1) {
            fprintf(fileout, "%.5lf ", A[i*column+j]);
        }
        fprintf(fileout, "\n");
    }
}

int main(void){
    FILE *filein = fopen("input.txt", "r");
    FILE *fileout = fopen("output.txt", "w");
    
    if(filein == NULL || fileout == NULL){
        return -1;
    }
    
    int a1,a2,a3;
    if(fscanf(filein, "%d %d %d", &a1,&a2,&a3)!=3){
        return -1;
    }
    fseek(filein, 0, SEEK_SET);
   
    int column, stroka;
    
    //code of errors
    int a,b;
    
    a = fscanf(filein, "%d", &stroka);
    b = fscanf(filein, "%d", &column);
    
    if((a != 1) || (b != 1)){
        return -1;
    }
    
    int count = column * stroka;
    printf("%d", count);
    
    if(count == 1){
        double oneE;
        int r = fscanf(filein, "%lf", &oneE);
        if(r == EOF || r == 0){
            return -1;
        }
        fprintf(fileout, "%.5lf", oneE);
        return 0;
    }
    
    double *A = (double*)malloc(count*sizeof(double));
    
    if(A == NULL){
        return -1;
    }
    
    if(setMatrix(filein, count, A) == -1){
        return -1;
    }
    
    int line = findLine(A, count, stroka, column);
    printf("%d \n", line);
    
    MatrixTrans(A, line, column);
    
    MatrixEnter(fileout, stroka, column, A);
    
    free(A);
    fclose(filein);
    fclose(fileout);
    return 0;
}




